import json
import pika
import django
import os
import sys
import time
from datetime import datetime
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendeess_bc.settings")
django.setup()

from attendees.models import AccountVO

# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_account(ch, method, properties, body):
#   content = load the json in body
    content = json.loads(conent)
#   first_name = content["first_name"]
    first_name = content['first_name']
#   last_name = content["last_name"]
    last_name = content['last_name']
#   email = content["email"]
    email = content['email']
#   is_active = content["is_active"]
    is_active = content['is_active']
#   updated_string = content["updated"]
    updated_string = content['updated']
#   updated = convert updated_string from ISO string to datetime
    updated = datetime.strptime(udpated_string, "%Y-%m-%dT%H:%M:%SZ")
#   if is_active:
#       Use the update_or_create method of the AccountVO.objects QuerySet
#           to update or create the AccountVO object
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            defaults={
                'first_name': first_name,
                'last_name': last_name,
                'email': email
            }
        )
#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists
    else:
        AccountVO.objects.delete()



# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
while True:
  try:
#       create the pika connection parameters
    parameters = pika.ConnectionParameters(host='rabbitmq')
#       create a blocking connection with the parameters
    connection = pika.BlockingConnection(parameters)
#       open a channel
    channel = connection.channel()
#       declare a fanout exchange named "account_info"
    channel.exchange_declare(exchange='logs', exchange_type='fanout')
#       declare a randomly-named queue
    result = channel.queue_declare(queue='', exclusive=True)
#       get the queue name of the randomly-named queue
    queue_name = result.method.queue
#       bind the queue to the "account_info" exchange
    channel.queue_bind(exchange='logs', queue=queue_name)

    def callback(ch, method, properties, body):
        print(' [x] %r' % body.decode())
#       do a basic_consume for the queue name that calls
    channel.basic_consume(
        # function above
        queue=queue_name,
        on_message_callback=callback,
        auto_ack=True
    )
#       tell the channel to start consuming
    channel.start_consuming()

#   except AMQPConnectionError
  except AMQPConnectionError:
      # print that it could not connect to RabbitMQ
    print('could not connect to RabbitMQ')
        # have it sleep for a couple of seconds
    time.sleep(2.0)
