from common.json import ModelEncoder
from django.http import JsonResponse
# from events.api_views import ConferenceListEncoder
from .models import Attendee, ConferenceVO, AccountVO
from django.views.decorators.http import require_http_methods
import json


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'name',
        'email',
    ]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        'name',
        'import_href'
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        'email',
        'name',
        'company_name',
        'created',
        'conference',
    ]
    encoders = {'conference': ConferenceVODetailEncoder(),}

    def get_extra_data(self, o):
        email_count = AccountVO.objects.filter(email=o.email).count()
        print(email_count)
        if email_count > 0:
            return {'has_account': True}
        else:
            return {'has_account': False}


@require_http_methods(['GET', 'POST'])
def api_list_attendees(request, conference_vo_id):
    if request.method == 'GET':

        attendees = Attendee.objects.filter(conference=conference_vo_id)
        attendee = Attendee.objects.get(id=1).conference.id
        # print(attendee, 'message1')
        print(ConferenceVO.objects.all())
        # print(attendees)
        # print(conference_vo_id)
        return JsonResponse(
            {'attendees': attendees},
            encoder=AttendeeEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        # print(content)
        print(ConferenceVO.objects.all())
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content['conference'] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid conference ID'},
                status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_attendee(request, id):
    if request.method == 'GET':
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
                {'attendee': attendee},
                encoder=AttendeeDetailEncoder,
                safe=False
        )
    elif request.method == 'DELETE':
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        try:
            if 'conference' in content:
                conference = ConferenceVO.objects.get(id=content['conference'])
                content['conference'] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid conference id'},
                status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )