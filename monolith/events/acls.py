import json
import requests
from .keys import PEXELS_API_KEY, WEATHER_API_KEY


def get_pic(city, state):
    # define api url
    api_url = 'https://api.pexels.com/v1/search'
    query = {
        "query": f"{city}, {state}"
    }
    headers = {'Authorization': PEXELS_API_KEY}
    response = requests.get(api_url, params=query, headers=headers) # noqa E501
    content = json.loads(response.content)
    try:
        picture_url = content['photos'][0]['src']['original']
        return {'picture_url': picture_url}
    except (KeyError, IndexError):
        return {'picture_url': None}

def get_weather(city, state):
    print(city, state)
    geocode_api_url = 'http://api.openweathermap.org/geo/1.0/direct'  # noqa E501'
# ?q={city}&{state}US&appid={WEATHER_API_KEY}
    query_geocode = {
        "q": f'{city},{state},US',
        "appid": WEATHER_API_KEY
    }
    # headers = WEATHER_API_KEY
    response = requests.get(geocode_api_url, params=query_geocode)
    content = json.loads(response.content) # takes JSON string and converts to Python (loads = load-string) # noqa E501
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
        print(lat, lon)
    except (KeyError, IndexError):
        return {
            'lat': None,
            'lon': None
        }

    query_current = {
        # "weather": f'{lat},{lon}',
        'units': 'imperial',
        "appid": WEATHER_API_KEY
    }

    current_api_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}'

    weather_response = requests.get(current_api_url, params=query_current)
    print(weather_response.content)
    weather_content = json.loads(weather_response.content)
    try:
        description = weather_content['weather'][0]['description']
        temp = weather_content['main']['temp']
        print(description, temp)
    except (KeyError, IndexError):
        return {'description': None, 'temp': None}

    return {
        'temp': temp,
        'description': description
    }

