import pika
import json


def send_message(presentation, body):
    # dictionary = {
    #     # 'name': presentation.presenter_name,
    #     # 'email': presentation.presenter_email,
    #     'status': status,
    # }
    # body = json.dumps(dictionary)
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=presentation)
    channel.basic_publish(
        exchange="",
        routing_key=presentation,
        body=json.dumps(body),
    )
    connection.close()
