# NOTES

## PEXELS
    - add photo_url to location model
    - get pexels url from webpage
    - in acl.py:
        - import requests
        - define ``def get_photo(city, state)``
        - url is everything before '?'
        - define a dictionary named query
            - query will be the key
            - value will be an f-string
                - f'{city}, {state}'
        - define headers to be authorization
            - in this case, ``{'Authorization': PEXELS_API_KEY}
        - define response as .get() or whatever, with params:
            - ``requests.get(url, params=query, headers=headers)``
        - define content as `json.load(response.content)`
        - call get_photo(city, state) with params (like NY, NY)
    - run URL in Insomnia
        - when the url is called in Insomnia, put city/state in the URL
        - don't put anything in body (yet)
        - look at the readout and find index of the thing you want
            - prints as a list of dictionaries
            - in this case, we want:
                - 'photos' > 'src' > 'original'
    - go back to acl.py
        - surround the following with
        - try:
            - define picture_url as ``content['photos'][0]['src']['original']``
            - return ``{'picture_url': picture_url}``
            - can test with ``print(picture_url)``
        - except (KeyError, IndexError):
            - return {'picture_url': None}
    - go to events/api_views.py
        - import .acls import get_photo()
        - in Location view?, above ``location = Location.etc``,
        - define photo = get_photo(content['city'], content['state'])
        - content.update(photo)
        - add picture_url to encoder




