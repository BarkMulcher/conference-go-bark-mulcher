import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    body = json.loads(body)
    name = body['presenter_name']
    title = body['title']
    email = body['presenter_email']
    print('working until here2')
    send_mail(
        'Your presentation has been accepted',
        f"{name}, we are happy to tell you that your presentation, {title}, has been accepted.",
        'admin@conference.go',
        [email],
        fail_silently=False
    )
    print(' received %r' % body)

def process_rejection(ch, method, properties, body):
    body = json.loads(body)
    name = body['presenter_name']
    title = body['title']
    email = body['presenter_email']
    print('working until here')
    send_mail(
        'your presentation has been rejected',
        f"{name}, we have rejected your presentation, titled {title}",
        'admin@conference.go',
        [email],
        fail_silently=False
    )
    print(' received %r' % body)


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True
        )

        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True
        )


        channel.start_consuming()
    except AMQPConnectionError:
        print('could not connect to RabbitMQ')
        time.sleep(2.0)


